// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './vuex/store'
import axios from './http'
import 'lib-flexible'
import * as filters from './assets/filter'
//引入使用element-ui
import ElementUI from 'element-ui';
import echarts from 'echarts'
//挂载在vue上，全局可以使用
Vue.use(ElementUI)

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

//axios，用于后端接口请求
Vue.prototype.$axios = axios
//echarts后边可视化会应用
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false
new Vue({
  el: '#app',
  store,
  router,
  components: {
    App
  },
  template: '<App/>'
})
